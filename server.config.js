const dotenv = require("dotenv");
const path = require("path");
const fs = require("fs");


function Config(options) {
    // package.json에 server

    this._options = Object.assign({},options) || {};
    this._isProd = process.argv.find((element) => element.toLowerCase() == "--prod");
}

Config.prototype.initEnviroment = function() {
    process.env.NODE_ENV =this._isProd ? "production":"development";

    /*
        실제 배포모드시 production.env 설정을 가져옴
    */
    if(this._isProd) {
        dotenv.config({path: path.join(__dirname, process.env.NODE_ENV + ".env")})
    } 
    /*
        개발 모드시 development.env 설정을 가져옴
    */
    else {
        dotenv.config({path: path.join(__dirname, process.env.NODE_ENV + ".env")})
    }
}



Config.prototype.initPath = function() {
    
    if(!this._options.path)
    {
        this._options.path = {};
    }

    const configPath = {};
    configPath.static = path.join(__dirname, this._options.path.static || "./");

    this._options.path = Object.assign({},configPath);
}

Config.prototype.getPath = function() {
    return this._options.path;
}



const obj = JSON.parse(fs.readFileSync(path.join(__dirname,"package.json"),"utf8")) || {};
const config = new Config(obj.serverConfig);

    
config.initEnviroment();
config.initPath();

module.exports = config;