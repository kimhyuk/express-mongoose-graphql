import mongoose from "mongoose";
import hasher from "../utils/hasher";
import debug from "debug";
import validator from "validator"

const _debug = debug("models","users");

//데이터 모델링
const userSchema =  mongoose.Schema({
    email: {type:String, unique:true},
    username: {type: String, unique:true},
    password: {type: String},
    admin: { type: Boolean, default: false},
    salt: { type: String }
});


/************* statics ****************/


// 사용자 정보로 DB검색
userSchema.statics.findByUsername = async function(username){
    return await this.findOne({ username:username });
}

userSchema.statics.findByUserEmail = async function(email){
    return await this.findOne({email:email});
}

userSchema.statics.create = async function({
    email,
    username,
    password
}) {
    if(!validator.isEmail(email))
    {
        throw new Error("이메일 형식이 옳지 않습니다.");
    } else if(typeof username !== "string") {
        throw new Error("이름이 옳지 않습니다.");
    } else if(!validator.isLength(password,{min:8,max:16})) {
        throw new Error("비밀번호 길이는 8~16자 입니다.");
    }

              
    const {hash,salt} = await this.generateHashAndSalt(password);    
            
    

    const user = new this({
        email:email.trim(),
        username:username.trim(),
        password:hash,
        salt
    });

    return user.save();
}

userSchema.statics.generateHashAndSalt = (password) => hasher({password});


/************* Methods ****************/

userSchema.methods.verifyPassword = async function(unauthorizedPassword) {
    const {password,salt} = this;
    
    try {
        const { hash } = await hasher({password:unauthorizedPassword,salt});
        return new Promise((resolve,_)=>resolve(hash === password));
    } catch(err) {
        console.error(err);
        return new Promise((_,reject)=>reject(err));
    }
}


const userModel = mongoose.model("User",userSchema);

export default userModel;