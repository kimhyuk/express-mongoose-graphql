import pbkdf2 from "pbkdf2-password";

const hasher = pbkdf2();

export default (options) => {
    return new Promise((resolve,reject)=>{
        hasher(options,(err,pass,salt,hash) => {
            !err ? resolve({pass,salt,hash}):reject(err);
        });
    });
}