import { default as typeDefs } from './typeDefs';
import { default as resolvers } from './resolvers';
import { default as middlewares} from "./middlewares";

export default {
    typeDefs,
    resolvers,
    middlewares
}